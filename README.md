# Recipe app api proxy

Nginx proxy app for our recipe app API

## Usage

### Env variables
* `LISTEN_PORT` - Port to listen on (defualt `8000`)
* `APP_HOST` - Hostname of the app to fwd requests to (default: `app`)
* `APP_PORT` - Port of the app to fwd requests to (default `9000`)
